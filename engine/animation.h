
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include <map>
#include <assimp/scene.h>
#include <iostream>
#include <time.h>

#include "indent.h"
#include "util.h"

template<class T>
struct keyframe {
	float time;
	T value;
};

class bone_animation {

	public:
		bone_animation();
		bone_animation(aiNodeAnim* anim, double tps, unsigned int dur);
		~bone_animation();
		glm::mat4 get_transform(float anim_time);

	private:
		std::vector<keyframe<glm::vec3>> loc;
		std::vector<keyframe<glm::quat>> rot;
		std::vector<keyframe<glm::vec3>> scale;
		unsigned int duration;

};

class animation {

	public:
		animation();
		animation(aiAnimation* anim);
		~animation();
		glm::mat4 get_transform(std::string bone_name);
		void play();
		bool is_playing();
		void update();

	private:
		std::map<std::string, bone_animation> bone_anims;
		unsigned int duration;
		unsigned int anim_time;
		int last_time;
		bool playing;
		double ticks_per_second;
};
