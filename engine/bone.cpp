#include "bone.h"

bone::bone(std::string arg_name, glm::mat4 bp, bone *arg_parent) {
	name = arg_name;
	offset_matrix = bp;
	parent = arg_parent;
}

bone::~bone() {

}

std::string bone::get_name() const {
	return name;
}

void bone::set_offset_matrix(glm::mat4 new_matrix) {
	offset_matrix = new_matrix;
}

void bone::set_node_matrix(glm::mat4 new_matrix) {
	node_matrix = new_matrix;
}

glm::mat4 bone::get_matrix() {
	/*if(!composite_bind_pose) {
		composite_bind_pose = parent == nullptr ? bind_pose : bind_pose * parent->get_matrix();
		//composite_bind_pose = bind_pose;
	}
	return composite_bind_pose.value_or(bind_pose);*/
	return glm::mat4();
}

glm::mat4 bone::rec_get_matrix(animation ani) {
	glm::mat4 pm = parent == nullptr ? glm::mat4() : parent->rec_get_matrix(ani);
	glm::mat4 ani_mat = node_matrix * ani.get_transform(name);
	return pm * ani_mat;
}

glm::mat4 bone::get_matrix(animation ani) {
	//glm::mat4 trans = glm::inverse(bind_pose) * ani.get_transform(name);
	glm::mat4 trans = rec_get_matrix(ani) * offset_matrix;
	//return parent == nullptr ? trans : parent->get_matrix(ani) * trans;
	return trans;
	//return glm::inverse(bind_pose) * trans;
	//return glm::mat4();
}

void bone::set_parent(bone* new_parent) {
	parent = new_parent;
}