#include <string>
#include <experimental/optional>
#include <glm/glm.hpp>
#include "animation.h"

class bone {
	public:
		bone(std::string arg_name, glm::mat4 bp = glm::mat4(1.0f), bone *arg_parent = nullptr);
		~bone();
		std::string get_name() const;
		void set_offset_matrix(glm::mat4 new_matrix);
		void set_node_matrix(glm::mat4 new_matrix);
		glm::mat4 get_matrix();
		glm::mat4 rec_get_matrix(animation ani);
		glm::mat4 get_matrix(animation ani);
		void set_parent(bone* new_parent);

	private:
		std::string name;
		glm::mat4 offset_matrix;
		glm::mat4 node_matrix;
		bone* parent;
		std::experimental::optional<glm::mat4> composite_bind_pose = {};
};