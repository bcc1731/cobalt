#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include "shader.h"
#include "camera.h"


/**
 *  Simple data struct representing a box in 3D space by storing all of its corner vertices.
 *  The struct obviously supports axis-aligned and non-aligned bounding boxes.
 */
struct box {
	glm::vec3 vertices[8]; ///< The corner vertices of the box.
};

/**
 *  A simple bounding box. It is stored using a position vector and three edge vectors, which
 *  makes it a Parallelepiped (https://en.wikipedia.org/wiki/Parallelepiped). Implemented for
 *  use in frustum computation for shadow map generation, might have some use for physics
 *  application later...
 *  \image html box.png
 */
class bounding_box {
	public:
		bounding_box();
		bounding_box(glm::vec3 min, glm::vec3 max);
		bounding_box(const box &arg_box);
		bounding_box(glm::vec3 direction);
		bounding_box(glm::vec3 direction, std::vector<glm::vec3> points);
		box get_vertices() const;
		void add_vertex(glm::vec3 vertex);
		void add_vertex(glm::vec3 vertex, uint i, bool neg_only = false);
		void add_bounding_box(const bounding_box& arg_box);
		void add_bounding_box(bounding_box arg_box, uint dimension, bool neg_only = false);
		glm::mat4 get_light_matrix();
		void render(camera_ptr cam);

		static shader_ptr box_shader;

	private:
		glm::vec3 position;
		glm::vec3 sides[3];
		bool no_points[3];
};

bounding_box operator * (glm::mat4 matrix, const bounding_box &arg_box);

#endif
