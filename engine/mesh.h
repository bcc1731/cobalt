//Class defining an object to be rendered
//Should be rendered as a part of the scene graph

#ifndef MESH_H
#define MESH_H

#define  GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <assimp/Importer.hpp> // C++ importer interface
#include <assimp/scene.h> // Output data structure
#include <assimp/postprocess.h> // Post processing flags

#include "material.h"
#include "node.h"
#include "scene.h"
#include "bounding_box.h"
#include "pointer_wrapper.h"
#include "gl_exception.h"
#include "shader.h"
#include "bone.h"

typedef pointer_wrapper<mesh> mesh_ptr;

#define MESH_INFO false     //Toggle detailed information output
//when loading meshes

/**
 *  Struct used to store vertex information. Keeps the position, uv coordinates, tangent, normal
 *  and the bone weights for each vertex.
 */
struct vertex_data {
	glm::vec3 position;
	glm::vec2 uv;
	glm::vec3 tangent;
	glm::vec3 normal;
	std::vector<std::pair<unsigned int, float>> weights;
};

class mesh_data {
	public:
		mesh_data();
		mesh_data(const string &scene_path, int model_index = 0);
		~mesh_data();
		void render();
		bounding_box get_bounding_box();
		string get_path();

	private:
        void load_model(aiMesh *inmesh);
		void buffer_vertices(bool keep_size = false);
	    std::vector<vertex_data> vertices;
	    std::vector<glm::vec3> faces;
        GLfloat *vertex_buffer;
        GLfloat *uv_buffer;
        GLfloat *normal_buffer;
        GLfloat *tangent_buffer;
        GLuint *bindex_buffer;
        GLfloat *bweight_buffer;
        unsigned int vertex_count;
        unsigned int uv_count;
        unsigned int normal_count;
        unsigned int tangent_count;
        GLuint vertex_array_object_id;
        GLuint vertex_id;
        GLuint uv_id;
        GLuint normal_id;
        GLuint tangent_id;
        GLuint bindex_id;
        GLuint bweight_id;
        bool has_uvs;
        string path;
        bounding_box box;
		std::map<std::string, animation> animations;
		std::vector<bone> bones;
		glm::mat4 global_inverse_transform;
		bool animated;
};

typedef pointer_wrapper<mesh_data> mesh_data_ptr;

/**
 *  Class representing a mesh object to be rendered. Every mesh is also a node in the scene graph
 *  and inherits all functionality from the node class. It contains all geometry data like vertices,
 *  faces, normals and tangents. Once implemented it will also contain all animation data associated
 *  with the mesh.
 */
class mesh : public node {
    public:
        mesh();
		mesh(mesh_data_ptr new_geometry);
        mesh(const std::string &file_path);
        mesh(const std::string &file_path, const std::string &arg_name);
        ~mesh();
        void load_model(const std::string &model_path);
        void load_model(const std::string &scene_path, int model_index);
        void set_material(std::shared_ptr<material> material);
        std::shared_ptr<material> get_material() const;
        void render();
        void render_no_bind();
        bool is_shadow_caster();
        bounding_box get_bounding_box();
        const string get_path();
		mesh_ptr clone();

    private:
		mesh_data_ptr geometry;
        material_ptr mat;
};

#endif // MESH_H
