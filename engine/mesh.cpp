#include "mesh.h"

mesh_data::mesh_data() {
	vertex_buffer = nullptr;
	uv_buffer = nullptr;
	tangent_buffer = nullptr;
	normal_buffer = nullptr;
	bindex_buffer = nullptr;
	bweight_buffer = nullptr;
	vertex_id = uv_id = tangent_id = normal_id = bindex_id = bweight_id = 0;
	vertex_array_object_id = 0;
	has_uvs = false;
	box = bounding_box();
	path = "";
	animated = false;
}

mesh_data::mesh_data(const std::string &scene_path, int model_index) : mesh_data() {
    std::clog << indent::get() << "- Loading mesh " << scene_path << " [" << model_index << "]\n";
	indent::increase();
    float start_time = glfwGetTime();

    Assimp::Importer importer;      //Create importer object
    const aiScene *scene = importer.ReadFile(scene_path,
                           aiProcess_CalcTangentSpace |
                           aiProcess_Triangulate |
                           aiProcess_JoinIdenticalVertices |
                           aiProcess_SortByPType |
                           aiProcess_GenSmoothNormals);    //Load scene

    if(!scene) {        //Check for loading errors
        std::cerr << indent::get() << "! Failed to load model " << scene_path << ": " << importer.GetErrorString();
    }

	if(scene->mNumAnimations > 0) {
		animated = true;
	}

    load_model(scene->mMeshes[model_index]);

	if(animated) {
		global_inverse_transform = glm::inverse(aiMatrix4x4ToGlm(&scene->mRootNode->mTransformation));

		// Resolve bone dependencies
		std::vector<aiNode*> tmpNodes{scene->mRootNode};
		while(tmpNodes.size() > 0) {

			aiNode* thisNode = tmpNodes[tmpNodes.size()-1];
			tmpNodes.pop_back();

			// Find our bone object for this node
			auto boneIt = std::find_if(bones.begin(), bones.end(), [&] (const bone & b){ return b.get_name().compare(thisNode->mName.C_Str())==0; } );

			// If it is a registered bone and has a parent bone...
			if(boneIt !=  bones.end() && thisNode->mParent != NULL) {

				// ...then we find the parent
				auto parentIt = std::find_if(bones.begin(), bones.end(), [&] (const bone & b){ return b.get_name().compare(thisNode->mParent->mName.C_Str())==0; } );
				if(parentIt == bones.end()) {
					bones.push_back(bone(thisNode->mParent->mName.C_Str(), glm::inverse(aiMatrix4x4ToGlm(&thisNode->mParent->mTransformation))));
					parentIt = std::find_if(bones.begin(), bones.end(), [&] (const bone & b){ return b.get_name().compare(thisNode->mParent->mName.C_Str())==0; } );
				}

				// And if we find it, we build the dependency
				(*boneIt).set_parent(&*parentIt);
			}

			// Then queue all children for the check.
			for(uint i = 0; i < thisNode->mNumChildren; i++) {
				tmpNodes.push_back(thisNode->mChildren[i]);
			}
		}

		for(unsigned int i = 0; i < scene->mNumAnimations; ++i) {
			aiAnimation* anim = scene->mAnimations[i];
			std::cout << indent::get() << "Found animation " << anim->mName.C_Str() << "\n";
			animations[anim->mName.C_Str()] = animation(anim);
		}
	}

	path = scene_path;

    if(DEBUG_INFO) std::clog << indent::get() << "- Finished loading mesh " /*<< model_path*/ << " with " /*<< vertices.size() << " vertices and " */ << vertex_count / 3 << " triangles in " << glfwGetTime() - start_time << " seconds\n\n";
	indent::decrease();
}

mesh_data::~mesh_data() {
    if(vertex_buffer != nullptr) delete[] vertex_buffer;
    if(uv_buffer != nullptr) delete[] uv_buffer;
    if(tangent_buffer != nullptr) delete[] tangent_buffer;
    if(normal_buffer != nullptr) delete[] normal_buffer;
    if(bindex_buffer != nullptr) delete[] bindex_buffer;
    if(bweight_buffer != nullptr) delete[] bweight_buffer;
    if(vertex_array_object_id != 0) glDeleteVertexArrays(1, &vertex_array_object_id);
    if(vertex_id != 0) glDeleteBuffers(1, &vertex_id);
    if(uv_id != 0) glDeleteBuffers(1, &uv_id);
    if(tangent_id != 0)glDeleteBuffers(1, &tangent_id);
    if(normal_id != 0) glDeleteBuffers(1, &normal_id);
    if(bindex_id != 0)glDeleteBuffers(1, &bindex_id);
    if(bweight_id != 0) glDeleteBuffers(1, &bweight_id);

    vertices.clear();
}

void mesh_data::render() {

	// Bind bone matrices for animation
	if(animated) {
		bool_uniform(true).bind("animated");

		animations.begin()->second.update();
		mat4_uniform matu;
		unsigned int index = 0;
		for(auto thisBone : bones) {
			// TODO: Active animation - don't just take the first.
			matu.set_data(thisBone.get_matrix(animations.begin()->second));
			matu.bind("bone_transforms["+std::to_string(index)+"]");
			++index;
		}
		uint_uniform(index).bind("bone_count");
	} else {
		bool_uniform(false).bind("animated");
	}

    glBindVertexArray(vertex_array_object_id);

	// Debug error Checking
	if(DEBUG_INFO) {
		GLint shader_id;
	    glGetIntegerv(GL_CURRENT_PROGRAM, &shader_id);
		glValidateProgram(shader_id);
		GLsizei length;
		char log_buffer[512];
		glGetProgramInfoLog(shader_id, 512, &length, log_buffer);
		if(length > 0) std::cerr << indent::get() << "! Shader error: " << log_buffer << '\n';
	}

    glDrawArrays(GL_TRIANGLES, 0, vertex_count / 3);    //draw the mesh

    glBindVertexArray(0);
}

bounding_box mesh_data::get_bounding_box() {
	return box;
}

string mesh_data::get_path() {
	return path;
}

/**
 *  Default constructor. Sets all values to zero-equivalent.
 */
mesh::mesh() : node() {
    node_matrix = glm::mat4(1.0f);
    parent_node = weak_ptr<node>();
}

mesh::mesh(mesh_data_ptr new_geometry) : mesh() {
	geometry = (new_geometry == nullptr ? mesh_data_ptr() : new_geometry);
}

/**
 *  Loading constructor. Initializes the object and loads the first mesh from the given file.
 *  @param	file_path	The path to the file you want to load the mes from.
 */
mesh::mesh(const std::string &file_path) : mesh() {
	load_model(file_path);
}

/**
 *  Naming constructor. Calls the loading constructor with the first argument and sets the
 *  name of the object to the second argument.
 *  @param	file_path	The path to the file you want to load the mes from.
 *  @param arg_name		The name that you want to assign to the mesh. Should be unique within
 *							the program. See named and name_manager for more detail.
 */
mesh::mesh(const std::string &file_path, const std::string &arg_name) : mesh(file_path) {
	set_name(arg_name);
}

/**
 *  Class destructor.
 */
mesh::~mesh() {
}

/**
 *  Loads a mesh from the given file. If the file contains more than one model, only the first one
 *  will be loaded. All previous data stored in this object will be deleted.
 *  @param	model_path	The path to the file holding the data.
 */
void mesh::load_model(const std::string &model_path) {
	geometry = mesh_data_ptr(model_path);
}

/**
 *  Loads the mesh with the specified index from the given file. The file is expected to contain
 *  a sufficient number of models.
 *  @param	scene_path	The path to the file holding the data.
 *  @param	model_index	The index of the model within the given scene.
 */
void mesh::load_model(const std::string &scene_path, int model_index) {
	geometry = mesh_data_ptr(scene_path, model_index);
}

/**
 *  Assigns the given material to this mesh. The material contains all data necessary for
 *  rendering (except for the geometry described in the mesh class). When the mesh is rendered
 *  the assigned material is bound, along with all its textures.
 *  @param	material	A shared_ptr to the material describing the mesh's render properties.
 */
void mesh::set_material(std::shared_ptr<material> material) {
    mat = material;
}

/**
 *  Returns a shared_ptr to the assigned material.
 */
std::shared_ptr<material> mesh::get_material() const {
    return mat;
}

/**
 *  Binds the assigned material and renders the mesh.
 */
void mesh::render() {
	if(geometry != nullptr) {
	    if(mat) {
	        mat->use();
	    } else {
	        std::cerr << indent::get() << "! Tried to render model without assigned material\n";
	        return;
	    }

		geometry->render();

		mat->unbind();
	} else {
		std::cerr << indent::get() << "! Tried to render model without assigned geometry\n";
	}
}

/**
 *  Renders the mesh without binding a material. Can be handy if the desired material is already
 *  active.
 */
void mesh::render_no_bind() {
	if(geometry != nullptr) {
		geometry->render();
	} else {
		std::cerr << indent::get() << "! Tried to render model without assigned geometry\n";
	}
}

/**
 *  Returns true if the material of the mesh casts shadows, false otherwise.
 */
bool mesh::is_shadow_caster() {
	return ((bool)mat && mat->is_shadow_caster());
}

/**
 *  Returns the bounding box of the mesh. The box is usually computed automatically when the mesh
 *  is loaded.
 */
bounding_box mesh::get_bounding_box() {
	return geometry!=nullptr ? geometry->get_bounding_box() : bounding_box();
}

/**
 *  Returns the path of the file from which the mesh was loaded. If the mesh was never loaded (e.g.
 *  procedurally generated) it will return the empty string.
 */
const string mesh::get_path() {
	return geometry!=nullptr ? geometry->get_path() : "";
}

mesh_ptr mesh::clone() {
	return geometry != nullptr ? std::make_shared<mesh>(geometry) : std::make_shared<mesh>();
}

//Private

/**
 *  Puts the mesh from the given aiMesh into engine format for later rendering. Computes an axis
 *  aligned bounding box and loads rigging and animation data (if implemented yet). Does not read
 *  or save any material, shader or texture data (but does save UV coordinates).
 */
void mesh_data::load_model(aiMesh *inmesh) {

    if(MESH_INFO)std::clog << indent::get() << "- Loading model\n";

    glm::vec3 min_vertex = glm::vec3(0, 0, 0), max_vertex = glm::vec3(0, 0, 0);

	if(inmesh->HasTextureCoords(0)) {
		has_uvs = true;
	} else {
		has_uvs = false;
		if(DEBUG_INFO) std::cerr << indent::get() << "! Could not find texture coordinates or generate tangents\n";
	}

    for(unsigned int i = 0; i < inmesh->mNumVertices; i++) { //Get vertices
		vertex_data vertex;
        vertex.position = glm::vec3(inmesh->mVertices[i].x, inmesh->mVertices[i].y, inmesh->mVertices[i].z); // Read and save vector position...
        if(MESH_INFO)std::clog << indent::get() << "- Read Vertex [" << i << "] at " << vertex.position.x << ", " << vertex.position.y << ", " << vertex.position.z << '\n';

		if(inmesh->HasTextureCoords(0)) {
	        has_uvs = true;
			vertex.uv = glm::vec2(inmesh->mTextureCoords[0][i].x, inmesh->mTextureCoords[0][i].y); // ... uv coordinates...
			if(MESH_INFO)std::clog << indent::get() << "- Read UV [" << i << "] at " << vertex.uv.x << ", " << vertex.uv.y << '\n';
		}

		if(inmesh->HasNormals()) {
            vertex.normal = glm::vec3(inmesh->mNormals[i].x, inmesh->mNormals[i].y, inmesh->mNormals[i].z); // .. normals...
            if(MESH_INFO)std::clog << indent::get() << "- Read Normal [" << i << "] at " << vertex.normal.x << ", " <<  vertex.normal.y << ", " << vertex.normal.z << '\n';
		} else {
	        if(DEBUG_INFO) std::cerr << indent::get() << "! Could not find normals\n";
		}

		if(inmesh->HasTangentsAndBitangents()) {
			vertex.tangent = glm::vec3(inmesh->mTangents[i].x, inmesh->mTangents[i].y, inmesh->mTangents[i].z);
            if(MESH_INFO)std::clog << indent::get() << "- Read Tangent [" << i << "] at " << vertex.tangent.x << ", " <<  vertex.tangent.y << ", " << vertex.tangent.z << '\n';
		}

		vertices.push_back(vertex);

        if(vertex.position.x < min_vertex.x) min_vertex.x = vertex.position.x;
        if(vertex.position.x > max_vertex.x) max_vertex.x = vertex.position.x;
        if(vertex.position.y < min_vertex.y) min_vertex.y = vertex.position.y;
        if(vertex.position.y > max_vertex.y) max_vertex.y = vertex.position.y;
        if(vertex.position.z < min_vertex.z) min_vertex.z = vertex.position.z;
        if(vertex.position.z > max_vertex.z) max_vertex.z = vertex.position.z;

    }
    box = bounding_box(min_vertex, max_vertex); // Generate AABB for mesh, used for scaling shadow map frustum

	if(animated) {
		// Load bones to animate mesh, if present
		for(unsigned int i = 0; i < inmesh->mNumBones; ++i) {
			aiBone *thisBone = inmesh->mBones[i];
			bones.push_back(bone(thisBone->mName.C_Str(), aiMatrix4x4ToGlm(&thisBone->mOffsetMatrix)));
			for(unsigned int j = 0; j < thisBone->mNumWeights; ++j) {
				auto weight = thisBone->mWeights[j];
				// Doesn't support more than 4 bones
				if(vertices[weight.mVertexId].weights.size() < 4) {
					// TODO: Should take the largest weights, not the first to appear
					vertices[weight.mVertexId].weights.push_back(std::pair<unsigned int, float>(
						bones.size(), weight.mWeight));
				}
			}
		}

		// Fill vertex-bone-weights so every vertex has 4
		for(auto&& v : vertices) {
			int diff = 4 - v.weights.size();
			for(int i = 0; i < diff; ++i) {
				v.weights.push_back(std::pair<unsigned int, float>(0, 0.0f));
			}
		}
	}

    for(unsigned int i = 0; i < inmesh->mNumFaces; i++) { //Get faces
        aiFace face = inmesh->mFaces[i];
        glm::vec3 vec = glm::vec3(face.mIndices[0], face.mIndices[1], face.mIndices[2]);    //Get faces and put them into vector
        faces.push_back(vec);
        if(MESH_INFO)std::clog << indent::get() << "- Read face [" << i << "] with vertices " << vec.x << ", " << vec.y << ", " << vec.z << '\n';
    }

	buffer_vertices();

}

/**
 *  Puts vertex data in OpenGL-readable float buffers.
 *  Doesn't need to be exectuted if data is already properly buffered (e.g. in last frame)
 *  @param	keep_size	If this is true, the old data will not be deleted, but simply overwritten
 *							with the new. If the array sizes mismatch there will be weird stuff
 *							going on...
 */
void mesh_data::buffer_vertices(bool keep_size) {

	// First, we declare local float buffers to get all our data into the right format
	std::vector<GLfloat> vbuffer;     //vertices
	std::vector<GLfloat> ubuffer;     //uvs
	std::vector<GLfloat> tbuffer;     //tangents
	std::vector<GLfloat> nbuffer;     //normals
	std::vector<GLuint> bbuffer;     //bone indices
	std::vector<GLfloat> wbuffer;     //bone weights

	// Now we go through all of our faces (which should all be triangles at this point), iterate
	// through their vertices and store their coordinates as three floats in a row. If we give
	// an array like that to OpenGL it will recreate the triangles.
	for(unsigned int i = 0; i < faces.size(); i++) {
		glm::vec3 face = faces[i];

		// face.xyz are the indices of the three vertices of our triangle.
		vbuffer.push_back(vertices[face.x].position.x);
		vbuffer.push_back(vertices[face.x].position.y);
		vbuffer.push_back(vertices[face.x].position.z);
		vbuffer.push_back(vertices[face.y].position.x);
		vbuffer.push_back(vertices[face.y].position.y);
		vbuffer.push_back(vertices[face.y].position.z);
		vbuffer.push_back(vertices[face.z].position.x);
		vbuffer.push_back(vertices[face.z].position.y);
		vbuffer.push_back(vertices[face.z].position.z);

		// If the mesh has UV data, then we also have tangent data (computed if not imported), so
		// we buffer both for later use.
		if(has_uvs) {
			ubuffer.push_back(vertices[face.x].uv.x);
			ubuffer.push_back(vertices[face.x].uv.y);
			ubuffer.push_back(vertices[face.y].uv.x);
			ubuffer.push_back(vertices[face.y].uv.y);
			ubuffer.push_back(vertices[face.z].uv.x);
			ubuffer.push_back(vertices[face.z].uv.y);

			tbuffer.push_back(vertices[face.x].tangent.x);
			tbuffer.push_back(vertices[face.x].tangent.y);
			tbuffer.push_back(vertices[face.x].tangent.z);
			tbuffer.push_back(vertices[face.y].tangent.x);
			tbuffer.push_back(vertices[face.y].tangent.y);
			tbuffer.push_back(vertices[face.y].tangent.z);
			tbuffer.push_back(vertices[face.z].tangent.x);
			tbuffer.push_back(vertices[face.z].tangent.y);
			tbuffer.push_back(vertices[face.z].tangent.z);
		}

		// The normalsare also stored in a separate array.
		nbuffer.push_back(vertices[face.x].normal.x);
		nbuffer.push_back(vertices[face.x].normal.y);
		nbuffer.push_back(vertices[face.x].normal.z);
		nbuffer.push_back(vertices[face.y].normal.x);
		nbuffer.push_back(vertices[face.y].normal.y);
		nbuffer.push_back(vertices[face.y].normal.z);
		nbuffer.push_back(vertices[face.z].normal.x);
		nbuffer.push_back(vertices[face.z].normal.y);
		nbuffer.push_back(vertices[face.z].normal.z);

		if(animated) {
			for(int i : {0, 1, 2, 3}) bbuffer.push_back(vertices[face.x].weights[i].first);
			for(int i : {0, 1, 2, 3}) bbuffer.push_back(vertices[face.y].weights[i].first);
			for(int i : {0, 1, 2, 3}) bbuffer.push_back(vertices[face.z].weights[i].first);

			for(int i : {0, 1, 2, 3}) wbuffer.push_back(vertices[face.x].weights[i].second);
			for(int i : {0, 1, 2, 3}) wbuffer.push_back(vertices[face.y].weights[i].second);
			for(int i : {0, 1, 2, 3}) wbuffer.push_back(vertices[face.z].weights[i].second);
		}
	}

	// If we know that the new data is the same size as the old one, we can simply reuse the memory.
	// Otherwise we delete the old data and reserve new space for our new data.
	if(!keep_size) {
		if(vertex_buffer != nullptr)delete[] vertex_buffer;
		vertex_buffer = new GLfloat[vbuffer.size()];
	}

	// We now copy the array from our std::vector into memory, "committing" to our structure.
	std::copy(vbuffer.begin(), vbuffer.end(), vertex_buffer);

	// Remember the length of our array to prevent overflows.
	vertex_count = vbuffer.size();

	// Clear the data in our temporary std::vector, we won't need it anymore.
	vbuffer.clear();

	// Now do thesame thing to UVs and tangents if they are present.
	if(has_uvs) {
		if(!keep_size) {
			if(uv_buffer != nullptr)delete[] uv_buffer;
			uv_buffer = new GLfloat[ubuffer.size()];
		}
		std::copy(ubuffer.begin(), ubuffer.end(), uv_buffer);
		uv_count = ubuffer.size();

		if(!keep_size) {
			if(tangent_buffer != nullptr)delete[] tangent_buffer;
			tangent_buffer = new GLfloat[tbuffer.size()];
		}
		std::copy(tbuffer.begin(), tbuffer.end(), tangent_buffer);
		tangent_count = tbuffer.size();
	} else {
		// If we don't have them, indicate it with a nullptr.
		if(uv_buffer != nullptr)delete[] uv_buffer;
		uv_buffer = nullptr;
		if(tangent_buffer != nullptr)delete[] tangent_buffer;
		tangent_buffer = nullptr;
	}
	ubuffer.clear();
	tbuffer.clear();

	// And now for the normals.
	if(!keep_size) {
		if(normal_buffer != nullptr)delete[] normal_buffer;
		normal_buffer = new GLfloat[nbuffer.size()];
	}
	std::copy(nbuffer.begin(), nbuffer.end(), normal_buffer);
	normal_count = nbuffer.size();
	nbuffer.clear();

	if(animated) {
		if(!keep_size) {
			if(bindex_buffer != nullptr) delete[] bindex_buffer;
			bindex_buffer = new GLuint[bbuffer.size()];
			if(bweight_buffer != nullptr) delete[] bweight_buffer;
			bweight_buffer = new GLfloat[wbuffer.size()];
		}
		std::copy(bbuffer.begin(), bbuffer.end(), bindex_buffer);
		bbuffer.clear();
		std::copy(wbuffer.begin(), wbuffer.end(), bweight_buffer);
		wbuffer.clear();
	} else {
		if(bindex_buffer != nullptr) delete[] bindex_buffer;
		bindex_buffer = nullptr;
		if(bweight_buffer != nullptr) delete[] bweight_buffer;
		bweight_buffer = nullptr;
	}

	// Create OpenGL buffers on GPU and buffer data
    if(vertex_id != 0) glDeleteBuffers(1, &vertex_id);
    glGenBuffers(1, &vertex_id);      //Buffer vertices
    glBindBuffer(GL_ARRAY_BUFFER, vertex_id);
    glBufferData(GL_ARRAY_BUFFER, vertex_count * sizeof(GLfloat), vertex_buffer, GL_STATIC_DRAW);

	if(normal_id != 0) glDeleteBuffers(1, &normal_id);
    glGenBuffers(1, &normal_id);      //Buffer normals
    glBindBuffer(GL_ARRAY_BUFFER, normal_id);
    glBufferData(GL_ARRAY_BUFFER, normal_count * sizeof(GLfloat), normal_buffer, GL_STATIC_DRAW);

    if(has_uvs) {
    	if(uv_id != 0) glDeleteBuffers(1, &uv_id);
        glGenBuffers(1, &uv_id);      //Buffer uvs
        glBindBuffer(GL_ARRAY_BUFFER, uv_id);
        glBufferData(GL_ARRAY_BUFFER, uv_count * sizeof(GLfloat), uv_buffer, GL_STATIC_DRAW);

		if(tangent_id != 0)glDeleteBuffers(1, &tangent_id);
        glGenBuffers(1, &tangent_id);       //Buffer tangents
        glBindBuffer(GL_ARRAY_BUFFER, tangent_id);
        glBufferData(GL_ARRAY_BUFFER, tangent_count * sizeof(GLfloat), tangent_buffer, GL_STATIC_DRAW);
    }

	if(animated) {
		if(bindex_id != 0) glDeleteBuffers(1, &bindex_id);
		glGenBuffers(1, &bindex_id);
		glBindBuffer(GL_ARRAY_BUFFER, bindex_id);
		glBufferData(GL_ARRAY_BUFFER, vertex_count/3*4 * sizeof(GLuint), bindex_buffer, GL_STATIC_DRAW);

		if(bweight_id != 0) glDeleteBuffers(1, &bweight_id);
		glGenBuffers(1, &bweight_id);
		glBindBuffer(GL_ARRAY_BUFFER, bweight_id);
		glBufferData(GL_ARRAY_BUFFER, vertex_count/3*4 * sizeof(GLfloat), bweight_buffer, GL_STATIC_DRAW);
	}

    glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Generate Vertex Array Object (VAO) and give it appropriate values.
	// Look at https://www.khronos.org/opengl/wiki/Vertex_Specification#Vertex_Array_Object for
	// more info on VAOs.
	if(vertex_array_object_id != 0) glDeleteVertexArrays(1, &vertex_array_object_id);
    glGenVertexArrays(1, &vertex_array_object_id);
    glBindVertexArray(vertex_array_object_id);

    glEnableVertexAttribArray(shader_vertex_location);       //Give vertices to OGL (location = 0)
    glBindBuffer(GL_ARRAY_BUFFER, vertex_id);
    glVertexAttribPointer(
        shader_vertex_location,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void *)0
    );

    glEnableVertexAttribArray(shader_normal_location);       //Give normals to OGL (location = 2)
    glBindBuffer(GL_ARRAY_BUFFER, normal_id);
    glVertexAttribPointer(
        shader_normal_location,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void *)0
    );

    if(has_uvs) {
        glEnableVertexAttribArray(shader_uv_location);       //Give uvs to OGL (location = 1)
        glBindBuffer(GL_ARRAY_BUFFER, uv_id);
        glVertexAttribPointer(
            shader_uv_location,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void *)0
        );

        glEnableVertexAttribArray(shader_tangent_location);       //Give tangents to OGL (location = 3)
        glBindBuffer(GL_ARRAY_BUFFER, tangent_id);
        glVertexAttribPointer(
            shader_tangent_location,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void *)0
        );
    }

	if(animated) {
        glEnableVertexAttribArray(shader_bone_index_location);       //Give bone indices to OGL
        glBindBuffer(GL_ARRAY_BUFFER, bindex_id);
        glVertexAttribPointer(
            shader_bone_index_location,
            4,
            GL_UNSIGNED_INT,
            GL_FALSE,
            0,
            (void *)0
        );

        glEnableVertexAttribArray(shader_bone_weight_location);       //Give bone weights to OGL
        glBindBuffer(GL_ARRAY_BUFFER, bweight_id);
        glVertexAttribPointer(
            shader_bone_weight_location,
            4,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void *)0
        );
	}

    glBindVertexArray(0);
}