#ifndef STD_SHADER_H_INCLUDED
#define STD_SHADER_H_INCLUDED

#include "../shader.h"

namespace cs {

class std_shader : public shader{
	public:
		std_shader(uint d_light_num = 0, uint p_light_num = 0, uint csm_levels = 3);
};

typedef pointer_wrapper<std_shader> std_shader_ptr;

}

#endif
