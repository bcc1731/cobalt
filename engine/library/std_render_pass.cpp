#include "std_render_pass.h"
using namespace cs;

#define bias glm::mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0)

const std::string shader_dir = HOME_DIR + string("engine/library/shader/");
const int shadow_map_resolution = 2048;
const glm::vec3 cubemap_directions[6] = {
	glm::vec3(1, 0, 0),
	glm::vec3(-1, 0, 0),
	glm::vec3(0, 1, 0),
	glm::vec3(0, -1, 0),
	glm::vec3(0, 0, 1),
	glm::vec3(0, 0, -1),
};
const glm::vec3 cubemap_ups[6] = {
	glm::vec3(0, -1, 0),
	glm::vec3(0, -1, 0),
	glm::vec3(0, 0, 1),
	glm::vec3(0, 0, -1),
	glm::vec3(0, -1, 0),
	glm::vec3(0, -1, 0),
};

std_render_pass::std_render_pass() :
		render_pass<scene, camera_ptr, std::vector<directional_light_ptr>, std::vector<point_light_ptr>>(shader_ptr()) {
	std_shader = std_shader_ptr(1, 1, CSM_LEVELS);
	depth_shader = shader_ptr(shader_dir + "depthOnlyVertexShader.glsl", shader_dir + "depthOnlyFragmentShader.glsl");
}

std_render_pass::~std_render_pass() {
}

void std_render_pass::render(scene &a_scene, camera_ptr the_camera, std::vector<directional_light_ptr> d_lights, std::vector<point_light_ptr> p_lights) {

	// Remove all nullptr entries from light arrays
	for(auto l = d_lights.begin(); l != d_lights.end(); ++l) {
		if(*l == nullptr) d_lights.erase(l);
	}
	for(auto l = p_lights.begin(); l != p_lights.end(); ++l) {
		if(*l == nullptr) p_lights.erase(l);
	}

	// Reload shaders and shadowmaps in case number of lights has changed
	if(d_lights.size() != num_d_lights || p_lights.size() != num_p_lights) {
		num_d_lights = d_lights.size();
		num_p_lights = p_lights.size();

		// Reload shaders
		std_shader = std_shader_ptr(num_d_lights, num_p_lights, CSM_LEVELS);

		// Generate directional shadow maps and framebuffers
		directional_shadow_maps.clear();
		directional_shadow_map_framebuffers.clear();
		directional_shadow_map_view_projections.clear();

		for(unsigned int i = 0; i < num_d_lights; ++i) {
			for(uint j = 0; j < CSM_LEVELS; j++) {
				auto shadow_map = std::make_shared<texture2d>(
					shadow_map_resolution, shadow_map_resolution, "", GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST);
				auto shadow_map_attachment = std::make_shared<texture_framebuffer_attachment>(shadow_map);
				auto shadow_map_framebuffer = std::make_shared<framebuffer>(
					framebuffer::attachments(), framebuffer::optional_attachment(shadow_map_attachment));

				directional_shadow_map_view_projections.push_back(get_light_matrix(d_lights[i]->get_direction(), a_scene, the_camera, j));

				directional_shadow_maps.push_back(shadow_map);
				directional_shadow_map_framebuffers.push_back(shadow_map_framebuffer);
			}
		}

		// Generate point-light shadow maps and framebuffers
		point_shadow_maps.clear();
		point_shadow_map_framebuffers.clear();
		for(unsigned int i = 0; i < num_p_lights; ++i) {
			auto shadow_map = std::make_shared<cubemap>(shadow_map_resolution/4.0f, shadow_map_resolution/4.0f, "", GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST);
			point_shadow_maps.push_back(shadow_map);

			for(GLenum face = GL_TEXTURE_CUBE_MAP_POSITIVE_X; face <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++face) {
				auto shadow_map_attachment = std::make_shared<texture_framebuffer_attachment>(shadow_map, face);
				point_shadow_map_framebuffers.push_back(std::make_shared<framebuffer>(framebuffer::attachments(), framebuffer::optional_attachment(shadow_map_attachment)));
			}
		}
	} else {
		// Update directional light matrices to fit the scene
		for(unsigned int i = 0; i < num_d_lights; ++i) {
			for(uint j = 0; j < CSM_LEVELS; j++) {
				directional_shadow_map_view_projections[i*CSM_LEVELS+j] = get_light_matrix(d_lights[i]->get_direction(), a_scene, the_camera, j);
			}
		}
	}

	// Save viewport variables
	int viewport_variables[4];
	glGetIntegerv(GL_VIEWPORT, viewport_variables);

	// Render directional shadow maps
	set_shader(depth_shader);
	prepare_render();
	glViewport(0, 0, shadow_map_resolution, shadow_map_resolution);
    GLint active_shader_id;
    glGetIntegerv(GL_CURRENT_PROGRAM, &active_shader_id);
	GLuint model_view_projection_id = glGetUniformLocation(active_shader_id, "model_view_projection_matrix");
	glDrawBuffer(GL_NONE);

	for(unsigned int i = 0; i < num_d_lights*CSM_LEVELS; ++i) {
		directional_shadow_map_framebuffers[i]->bind();
        glClear(GL_DEPTH_BUFFER_BIT);
		for(auto node : a_scene.enumerate_nodes()) {
			if(mesh *m = dynamic_cast<mesh*>(node.get())) {
				if(m->is_shadow_caster()) {
					auto model_view_projection = directional_shadow_map_view_projections[i] * m->get_node_matrix();
					glUniformMatrix4fv(model_view_projection_id, 1, GL_FALSE, &model_view_projection[0][0]);
					m->render_no_bind();
				}
			}
		}
	}
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	//Render point light shadow maps
	glViewport(0, 0, shadow_map_resolution/4, shadow_map_resolution/4);
	assert(point_shadow_map_framebuffers.size() == 6 * num_p_lights);
	for(unsigned int i = 0; i < num_p_lights; ++i) {
		glm::mat4 projection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, p_lights[i]->get_radius());
		for(unsigned int j = 0; j < 6; ++j) {
			point_shadow_map_framebuffers[i * 6 + j]->bind();
		    glClear(GL_DEPTH_BUFFER_BIT);
		    glm::mat4 view_projection = projection * glm::lookAt(p_lights[i]->get_position(), p_lights[i]->get_position() + cubemap_directions[j], cubemap_ups[j]);
		    for(auto node : a_scene.enumerate_nodes()) {
				if(mesh *m = dynamic_cast<mesh*>(node.get())) {
					if(m->is_shadow_caster()) {
						auto model_view_projection = view_projection * m->get_node_matrix();
						glUniformMatrix4fv(model_view_projection_id, 1, GL_FALSE, &model_view_projection[0][0]);
						m->render_no_bind();
					}
				}
			}
		}
	}

	// Render scene
	set_shader(std_shader);
	prepare_render();
    framebuffer::get_screen()->bind();
    glViewport(viewport_variables[0], viewport_variables[1], viewport_variables[2], viewport_variables[3]);
    glDrawBuffer(GL_BACK);

	// Calculate and set general matrix uniforms
    glGetIntegerv(GL_CURRENT_PROGRAM, &active_shader_id);
    glm::mat4 view_matrix = the_camera->get_view();
    glm::mat4 inverse_view_matrix = glm::inverse(view_matrix);

    GLuint view_id = glGetUniformLocation(active_shader_id, "view");
    glUniformMatrix4fv(view_id, 1, GL_FALSE, &view_matrix[0][0]);
    GLuint inverse_view_id = glGetUniformLocation(active_shader_id, "inverse_view");
    glUniformMatrix4fv(inverse_view_id, 1, GL_FALSE, &inverse_view_matrix[0][0]);
    GLuint projection_id = glGetUniformLocation(active_shader_id, "projection");
    glUniformMatrix4fv(projection_id, 1, GL_FALSE, &the_camera->get_projection()[0][0]);
    GLuint model_id = glGetUniformLocation(active_shader_id, "model");
    GLuint normal_id = glGetUniformLocation(active_shader_id, "normal_to_view_matrix");

	// The z values of the near and far clipping planes
	glm::mat4 inv_proj = glm::inverse(the_camera->get_projection());
	glm::vec2 zplanes;
	glm::vec4 tmp = inv_proj * glm::vec4(0, 0, -1, 1);
	zplanes.x = tmp.z / tmp.w;
	tmp = inv_proj * glm::vec4(0, 0, 1, 1);
	zplanes.y = tmp.z / tmp.w - zplanes.x;
	vec2_uniform(zplanes).bind("zplanes");


    // Set shadow uniforms
    for(unsigned int i = 0; i < num_d_lights*CSM_LEVELS; ++i) {
    	GLuint mat_id = glGetUniformLocation(active_shader_id, ("shadow_map_view_projection_matrix[" + std::to_string(i) + "]").c_str());
    	glUniformMatrix4fv(mat_id, 1, GL_FALSE, &(bias * directional_shadow_map_view_projections[i])[0][0]);
    }

    for(auto node : a_scene.enumerate_nodes()) {
        if(mesh *m = dynamic_cast<mesh*>(node.get())) {
            if(m->get_material() != nullptr && m->get_material()->is_standard()) {
            	for(unsigned int i = 0; i < directional_shadow_maps.size(); ++i) {
            		m->get_material()->add_texture("directional_shadow_map[" + std::to_string(i) + "]", directional_shadow_maps[i]);
            	}
            	for(unsigned int i = 0; i < point_shadow_maps.size(); ++i) {
            		m->get_material()->add_texture("point_shadow_map[" + std::to_string(i) + "]", point_shadow_maps[i]);
            	}
            }
        }
    }

	// Set directional light uniforms
    for(unsigned int i = 0; i < d_lights.size(); ++i) {
        directional_light &d_light = *d_lights[i];
        glm::vec3 color_vec = d_light.get_color() * clamp(d_light.get_intensity(), 0.0f, 1.0f);
        glm::vec3 direction_vec = glm::vec3((view_matrix * glm::vec4(d_light.get_direction(), 0.0f)));
        std::string is = std::to_string(i);
        glUniform3f(glGetUniformLocation(active_shader_id, ("directional_light_colors[" + is + "]").c_str()),
                    color_vec.x, color_vec.y, color_vec.z);
        glUniform3f(glGetUniformLocation(active_shader_id, ("directional_light_directions[" + is + "]").c_str()),
                    direction_vec.x, direction_vec.y, direction_vec.z);
    }

	// Set point light uniforms
    for(unsigned int i = 0; i < p_lights.size(); ++i) {
        point_light &p_light = *p_lights[i];
        glm::vec3 point_color_vec = p_light.get_color() * p_light.get_intensity();
        glm::vec3 point_position_vec = glm::vec3((view_matrix * glm::vec4(p_light.get_position(), 1.0f)));
        std::string is = std::to_string(i);
        glUniform3f(glGetUniformLocation(active_shader_id, ("point_light_colors[" + is + "]").c_str()),
                    point_color_vec.x, point_color_vec.y, point_color_vec.z);
        glUniform3f(glGetUniformLocation(active_shader_id, ("point_light_positions[" + is + "]").c_str()),
                    point_position_vec.x, point_position_vec.y, point_position_vec.z);
        float r = p_light.get_radius();
        glUniform1f(glGetUniformLocation(active_shader_id, ("point_light_radii_sq[" + is + "]").c_str()), r * r);
    }

	// Render individual meshes
    for(auto node : a_scene.enumerate_nodes()) {
        if(mesh *m = dynamic_cast<mesh*>(node.get())) {
            if(m->get_material() != nullptr && m->get_material()->is_standard()) {
                glm::mat4 render_model = m->get_node_matrix();
                glm::mat3 normal_matrix = glm::transpose(glm::inverse(glm::mat3(view_matrix * render_model)));
                glUniformMatrix4fv(model_id, 1, GL_FALSE, &render_model[0][0]);
				glUniformMatrix3fv(normal_id, 1, GL_FALSE, &normal_matrix[0][0]);
                m->render();
            }
        }
    }
    /*for(auto node : a_scene.enumerate_nodes()) {
        if(mesh *m = dynamic_cast<mesh*>(node.get())) {
            if(m->get_material() != nullptr && m->get_material()->is_standard()) {
				bounding_box box = m->get_node_matrix() * m->get_bounding_box();
				box.render(the_camera);
			}
		}
	}*/
}

glm::mat4 std_render_pass::get_light_matrix(glm::vec3 direction, scene& a_scene, camera_ptr cam, uint level) {
	//bounding_box light_box = bounding_box(direction);
	float zmin = (level == CSM_LEVELS-1 ? 0 : 1.f/float(pow(3, level+1))), zmax = 1.f/float(pow(3, level)); // Find min and max dist from camera for levels in [0, 1]
	glm::vec3 close_verts[4] = {
		glm::vec3(-1, -1, -1),
		glm::vec3(1, -1, -1),
		glm::vec3(-1, 1, -1),
		glm::vec3(1, 1, -1)
	};
	glm::vec3 far_verts[4] = {
		glm::vec3(-1, -1, 1),
		glm::vec3(1, -1, 1),
		glm::vec3(-1, 1, 1),
		glm::vec3(1, 1, 1)
	};

	glm::mat4 inv_proj = glm::inverse(cam->get_view())*glm::inverse(cam->get_projection());
	std::vector<glm::vec3> box_verts;
	for(uint i=0; i < 4; i++) {
		glm::vec4 tmp_vec = inv_proj * glm::vec4(close_verts[i], 1);
		close_verts[i] = glm::vec3(tmp_vec)/tmp_vec.w;
		tmp_vec = inv_proj * glm::vec4(far_verts[i], 1);
		far_verts[i] = glm::vec3(tmp_vec)/tmp_vec.w - close_verts[i];
		box_verts.push_back(close_verts[i]+zmin*far_verts[i]);
		box_verts.push_back(close_verts[i]+zmax*far_verts[i]);
	}
	// TODO: Only consider objects that would actually be in the box
	bounding_box light_box(direction, box_verts);
	for(auto node : a_scene.enumerate_nodes()) {
		if(mesh *m = dynamic_cast<mesh*>(node.get())) {
			if(m->is_shadow_caster()) {
				light_box.add_bounding_box(m->get_node_matrix() * m->get_bounding_box(), 2, true);
			}

		}
	}
	return light_box.get_light_matrix();
}
