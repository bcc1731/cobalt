#version 130
#define MAX_BONES 64

in vec3 vertex_position;
in vec4 bone_indices;
in vec4 bone_weights;

uniform mat4 model_view_projection_matrix;
uniform mat4 bone_transforms[MAX_BONES];
uniform int bone_count;
uniform bool animated;

void main() {
	vec4 position = vec4(vertex_position, 1.0);

	if(animated) {
		mat4 transform = mat4(1.0);

		transform =
			bone_weights.x * bone_transforms[int(bone_indices.x)-1] * transform +
			bone_weights.y * bone_transforms[int(bone_indices.y)-1] * transform +
			bone_weights.z * bone_transforms[int(bone_indices.z)-1] * transform +
			bone_weights.w * bone_transforms[int(bone_indices.w)-1] * transform;

			position = transform * position;
	}


	gl_Position = model_view_projection_matrix * position;
}
