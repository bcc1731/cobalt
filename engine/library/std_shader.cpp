#include "std_shader.h"
using namespace cs;

const std::string shader_dir = HOME_DIR + string("engine/library/shader/");

std_shader::std_shader(uint d_light_num, uint p_light_num, uint csm_levels) : shader(
		shader(shader_dir + "std_shader.vertex", shader_dir + "std_shader.fragment",
			std::string("#version 430\n#define NUM_DIRECTIONAL_LIGHTS " + std::to_string(d_light_num) + "\n#define CSM_LEVELS " + std::to_string(csm_levels) + "\n#define NUM_POINT_LIGHTS " + std::to_string(p_light_num) + "\n"))) {

}
