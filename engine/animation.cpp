#include "animation.h"
#include "glm/gtx/string_cast.hpp"

bone_animation::bone_animation() {
	// Constructor
}

bone_animation::bone_animation(aiNodeAnim* anim, double tps, unsigned int dur) {

	// Location keyframes
	for(unsigned int i = 0; i < anim->mNumPositionKeys; ++i) {
		auto key = anim->mPositionKeys[i];
		loc.push_back(keyframe<glm::vec3>{float(key.mTime/tps*1000.f), aiVec3DToGlm(&key.mValue)});
	}

	// Rotation keyframes
	for(unsigned int i = 0; i < anim->mNumRotationKeys; ++i) {
		auto key = anim->mRotationKeys[i];
		rot.push_back(keyframe<glm::quat>{float(key.mTime/tps*1000.f), aiQuatToGlm(&key.mValue)});
	}

	// Scale keyframes
	for(unsigned int i = 0; i < anim->mNumScalingKeys; ++i) {
		auto key = anim->mScalingKeys[i];
		scale.push_back(keyframe<glm::vec3>{float(key.mTime/tps*1000.f), aiVec3DToGlm(&key.mValue)});
	}

	duration = dur;
}

bone_animation::~bone_animation() {
	// Destructor
}

glm::mat4 bone_animation::get_transform(float anim_time) {
	//return glm::scale(glm::vec3(anim_time/500.f, anim_time/500.f, anim_time/500.f)) * glm::rotate(anim_time/100.f, glm::vec3(1.0, 0, 0));
	int index = -1;
	glm::mat4 loc_mat, rot_mat, scale_mat;

	// Location
	for(auto frame : loc) {
		index++;
		if(frame.time >= anim_time) {
			break;
		}
	}

	if(index == -1) {
		std::cerr << indent::get() << "! Tried to get animation transform without location keyframes\n";
		loc_mat = glm::mat4(1.0f);
	} else {
		loc_mat = glm::translate(loc[index].value);
	}

	// Rotation
	index = -1;
	for(auto frame : rot) {
		index++;
		if(frame.time >= anim_time) {
			break;
		}
	}

	if(index == -1) {
		std::cerr << indent::get() << "! Tried to get animation transform without rotation keyframes\n";
		rot_mat = glm::mat4(1.0f);
	} else {
		int prev = (index-1)%rot.size();
		float prev_time = rot[prev].time;
		float fac = (anim_time-prev_time)/(rot[index].time-prev_time);
		rot_mat = mat4_cast((1-fac)*rot[prev].value + fac*rot[index].value);
	}

	// Scale
	index = -1;
	for(auto frame : scale) {
		index++;
		if(frame.time >= anim_time) {
			break;
		}
	}

	if(index == -1) {
		std::cerr << indent::get() << "! Tried to get animation transform without scale keyframes\n";
		scale_mat = glm::mat4(1.0f);
	} else {
		scale_mat = glm::scale(scale[index].value);
	}

	return loc_mat * rot_mat * scale_mat;
}

//---------------------------------------------------------------------------------------- ANIMATION

animation::animation() {
	playing = false;
}

animation::animation(aiAnimation* anim) {
	playing = false;

	ticks_per_second = anim->mTicksPerSecond;
	duration = anim->mDuration/ticks_per_second*1000.0f; // Duration in milliseconds

	for(unsigned int i = 0; i < anim->mNumChannels; ++i) {
		auto channel = anim->mChannels[i];
		bone_anims[channel->mNodeName.C_Str()] = bone_animation(channel, ticks_per_second, duration);
	}
}

animation::~animation() {
	// Destructor
}

glm::mat4 animation::get_transform(std::string bone_name) {
	return (bone_anims.find(bone_name) != bone_anims.end()) ? bone_anims[bone_name].get_transform(anim_time) : glm::mat4();
}

void animation::play() {
	last_time = time_millisecs();
	anim_time = 0.0f;
	playing = true;
}

bool animation::is_playing() {
	return playing;
}

void animation::update() {
	int tmp_time = time_millisecs();
	anim_time += tmp_time - last_time;
	anim_time = anim_time % duration;
	last_time = tmp_time;
}