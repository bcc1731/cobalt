#include "cobalt.h"
#include "library/standard.h"
#include "glm/ext.hpp"

#define RANDFLOAT (static_cast <float> (rand()) / static_cast <float> (RAND_MAX) -0.5)

const float VEER_SPEED = .003f;
const float ACCEL_SPEED = .05f;
const uint ASTEROID_COUNT = 100;

int main() {

	// Create window
	window win(1280, 620, "Cobalt Space Demo");

    //init framerate counting
    int fps = 0, fpsc = glfwGetTime();
	glfwSwapInterval(0);

	// Load scene
	cs::std_scene scene;
	scene.load("space_demo.scene");

	auto mat = std::make_shared<cs::std_material>();
	mat->set_color_map("demo/res/textures/asteroid.png");
	mat->set_shader_mask("demo/res/textures/asteroid_sm.png");
	mat->set_luminosity(.5f);

	mesh_ptr asteroids[ASTEROID_COUNT];
	asteroids[0] = mesh_ptr("demo/res/models/asteroid.dae");
	for(uint i = 0; i < ASTEROID_COUNT; i++) {
		asteroids[i] = asteroids[0]->clone();
		asteroids[i]->set_material(mat);
		asteroids[i]->place(RANDFLOAT * 100.f, RANDFLOAT * 100.f, RANDFLOAT * 100.f);
		scene.append_node(asteroids[i]);
	}

	// Exit condition
	bool quit = false;

	// Game variables
	glm::vec3 rot_vel;
	glm::vec3 trans_vel;

	node_ptr camera_node = scene.find_node("camera_node");
	camera_ptr camera = scene.find_node("main_camera");
	camera->place(0, 3, 10);
	camera->look_at(0, 0, 0);
	camera->set_projection(glm::infinitePerspective(45.f, 16.f/9.f, .01f));
	camera->set_projection(glm::perspective(45.f, 16.f/9.f, .01f, 500.f));

	// Main loop
	while(!quit && !win.key_pressed(GLFW_KEY_ESCAPE) && !win.should_close()) {

		if(win.key_pressed(GLFW_KEY_A)) rot_vel.y += VEER_SPEED;
		if(win.key_pressed(GLFW_KEY_D)) rot_vel.y -= VEER_SPEED;
		if(win.key_pressed(GLFW_KEY_W)) rot_vel.x -= VEER_SPEED;
		if(win.key_pressed(GLFW_KEY_S)) rot_vel.x += VEER_SPEED;
		if(win.key_pressed(GLFW_KEY_E)) rot_vel.z -= VEER_SPEED;
		if(win.key_pressed(GLFW_KEY_Q)) rot_vel.z += VEER_SPEED;
		camera_node->rotate(rot_vel);
		rot_vel = .9f*rot_vel;

		if(win.key_pressed(GLFW_KEY_I)) trans_vel -= glm::vec3(camera_node->get_node_matrix() * glm::vec4(0, 0, ACCEL_SPEED, 0));
		if(win.key_pressed(GLFW_KEY_K)) trans_vel += glm::vec3(camera_node->get_node_matrix() * glm::vec4(0, 0, ACCEL_SPEED, 0));
		if(win.key_pressed(GLFW_KEY_J)) trans_vel -= glm::vec3(camera_node->get_node_matrix() * glm::vec4(ACCEL_SPEED, 0, 0, 0));
		if(win.key_pressed(GLFW_KEY_L)) trans_vel += glm::vec3(camera_node->get_node_matrix() * glm::vec4(ACCEL_SPEED, 0, 0, 0));
		if(win.key_pressed(GLFW_KEY_U)) trans_vel -= glm::vec3(camera_node->get_node_matrix() * glm::vec4(0, ACCEL_SPEED, 0, 0));
		if(win.key_pressed(GLFW_KEY_O)) trans_vel += glm::vec3(camera_node->get_node_matrix() * glm::vec4(0, ACCEL_SPEED, 0, 0));
		trans_vel = .9f*trans_vel;
		camera_node->move(trans_vel);


		// Rendering the scene
		win.clear();
		scene.render();
		win.update();

        // Checking for OpenGL errors (usually shader errors)
        try {
            check_gl_error();
        } catch(gl_exception &e) {
            std::cerr << "! Detected GL Error: " << e.what() << '\n';
            quit = true;
        }

        //count framerate
        if(glfwGetTime() - fpsc >= 1.0f) {
            std::clog << "-FPS: " << fps << "   \r";
            fps = 0;
            fpsc = glfwGetTime();
        } else {
            fps += 1;
        }

	}
}
